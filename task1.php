<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task 1</title>
</head>
<body>
<script language="JavaScript">

    var level = 0;
    document.write("<pre>");
    function printSpaces(){
        for (var i=1;i<=8*level;i++){
            document.write("&nbsp");
        }
    }

    function printAnyArray(currentArray){
        for(var i=0; i<currentArray.length; i++){
            if(Array.isArray(currentArray[i]))
            {
                level++;
                printAnyArray(currentArray[i])
            }
            else
            {
                printSpaces();
                document.write(currentArray[i]+"<br>");
            }
        }
        level--;
    }
    var givenArray = [23, 6, [2, [6, 2, 1, 2], 2], 5, 2];

    printAnyArray(givenArray);

    document.write("</pre>");
</script>

</body>
</html>